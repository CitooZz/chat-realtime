from django.conf.urls import patterns, include, url
from chat.apps.account.views import LoginView, SignupView, logout

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'chat.views.home', name='home'),
    # url(r'^chat/', include('chat.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    url(r"^account/login/$", LoginView.as_view(), name="account_login"),
    url(r"^account/signup/$", SignupView.as_view(), name="account_signup"),
    url(r"logout/$", logout, name="account_logout"),

    url(r"^people/", include("chat.apps.profile.urls")),
    url(r"^people/", include("chat.apps.friend.urls")),
    url(r"^messages/", include("chat.apps.message.urls")),

    url(r"^account/", include("account.urls")),
    url(r'^avatar/', include('avatar.urls')),
    url(r"^", include("chat.apps.dashboard.urls")),
    
)
