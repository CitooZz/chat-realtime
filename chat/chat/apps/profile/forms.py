from django import forms
from chat.apps.profile.models import Profile


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ["full_name", "gender", "location", "country", "about"]

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields["full_name"].required = True

    def clean_full_name(self):
        full_name = self.cleaned_data.get('full_name').strip()
        if not full_name:
            return forms.ValidationError("This field is required.")
        return full_name
