from django.conf.urls import patterns, url

urlpatterns = patterns(
	"chat.apps.profile.views",

	url(r"^profile/edit/$", "edit_profile", name="edit_profile"),
)