from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from chat.apps.profile.forms import ProfileForm


@login_required
def edit_profile(request):
    if request.POST:
        form = ProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            messages.success(request, _("Profile updated."))
    else:
        form = ProfileForm(instance=request.user.profile)

    return render(request, "profile/edit_profile.html", {'form': form})
