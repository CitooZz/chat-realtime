from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from django.db.models.signals import post_save


class Gender:
    MALE = 1
    FEMALE = 2

GENDER_CHOICES = (
    (Gender.MALE, "Male"),
    (Gender.FEMALE, "Female")
)


class Profile(models.Model):

    class Meta:
        app_label = "profile"

    user = models.OneToOneField(User, unique=True, verbose_name="user", related_name="profile")
    full_name = models.CharField( _("Full Name"), max_length=30, null=True, blank=True)
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, default=Gender.MALE)
    location = models.CharField(max_length=50, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    about = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.full_name


def create_profile_for_new_user(sender, instance=None, **kwargs):
    if instance is None:
        return

    profile, created = Profile.objects.get_or_create(user=instance)
    if created:
        profile.full_name = "%s %s" % (instance.first_name, instance.last_name)
        profile.save()

post_save.connect(create_profile_for_new_user, sender=User)
