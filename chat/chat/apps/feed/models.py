from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class FeedStatusManager(models.Manager):

    def feed_for_user(self, user):
        return self.filter(user=user).order_by('timestamp')


class FeedStatus(models.Model):
    user = models.ForeignKey(User, related_name="status")
    status = models.TextField()
    timestamp = models.DateTimeField(default=datetime.now())
    objects = FeedStatusManager()

    def __unicode__(self):
        return self.status


class FeedStatusComment(models.Model):
    feed_status = models.ForeignKey(FeedStatus, related_name="comments")
    comment = models.CharField(max_length=200)
    timestamp = models.DateTimeField(default=datetime.now())

    class Meta:
        ordering = ["-timestamp"]

    def __unicode__(self):
        return self.comment
