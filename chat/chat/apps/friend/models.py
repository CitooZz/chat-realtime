from django.db import models
from django.contrib.auth.models import User


class FriendShipManager(models.Manager):

    def friendships_for_user(self, user):
        friends = []
        for friendship in self.filter(from_user=user).select_related(depth=1):
            friends.append(
                {"friend": friendship.to_user, "friendship": friendship})
        for friendship in self.filter(to_user=user).select_related(depth=1):
            friends.append(
                {"friend": friendship.from_user, "friendship": friendship})
        return friends

    def friends_for_user(self, user):
        friends = []
        for friendship in self.filter(from_user=user).select_related(depth=1):
            friends.append(friendship.to_user)
        for friendship in self.filter(to_user=user).select_related(depth=1):
            friends.append(friendship.from_user)

        return friends

    def are_friends(self, user1, user2):
        if self.filter(from_user=user1, to_user=user2).count() > 0:
            return True
        if self.filter(from_user=user2, to_user=user1).count() > 0:
            return True
        return False

    def remove(self, user1, user2):
        if self.filter(from_user=user1, to_user=user2):
            friendship = self.filter(from_user=user1, to_user=user2)
        elif self.filter(from_user=user2, to_user=user1):
            friendship = self.filter(from_user=user2, to_user=user1)
        friendship.delete()


class FriendShip(models.Model):

    """
    A friendship is a bi-directional association between two users who
    have both agreed to the association.
    """

    to_user = models.ForeignKey(User, related_name="friends")
    from_user = models.ForeignKey(User, related_name="_unused_")
    added = models.DateTimeField(auto_now=True)

    objects = FriendShipManager()

    class Meta:
        unique_together = (("to_user", "from_user"),)


class FriendShipInvitationStatus:
    SENT = 1
    ACCEPT = 2
    DECLINED = 3


FRIENDSHIP_INVITATION_STATUS = (
    (FriendShipInvitationStatus.SENT, "Sent"),
    (FriendShipInvitationStatus.ACCEPT, "Accept"),
    (FriendShipInvitationStatus.DECLINED, "Declined")
)


class FriendShipInvitation(models.Model):

    """
    a friendship invite is an invitation from user to another to be associated as friends
    """

    to_user = models.ForeignKey(User, related_name="invitation_from")
    from_user = models.ForeignKey(User, related_name="invitation_to")
    message = models.TextField()
    sent = models.DateTimeField(auto_now=True)
    status = models.SmallIntegerField(
        choices=FRIENDSHIP_INVITATION_STATUS, default=FriendShipInvitationStatus.SENT)
