from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from chat.apps.profile.models import Profile
from chat.apps.friend.models import FriendShip, FriendShipInvitation


@login_required
def browse_people(request):
    peoples = Profile.objects.exclude(user=request.user)
    context = {
        'peoples': peoples
    }
    return render(request, 'friend/browse_friend.html', context)


@login_required
def add_friend(request, username):
    from_user = request.user
    to_user = get_object_or_404(User, username=username)
    if not FriendShip.objects.are_friends(from_user, to_user) and from_user != to_user:
        invitation = FriendShipInvitation(from_user=from_user, to_user=to_user)
        invitation.save()
        messages.success(request, "This friend has been added.")
    

    return redirect(reverse("browse_people"))
