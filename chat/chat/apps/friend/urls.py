from django.conf.urls import patterns, url

urlpatterns = patterns(
	"chat.apps.friend.views",

	url(r"^browse/$", "browse_people", name="browse_people"),
	url(r"^add_friend/([\w\._-]+)$", "add_friend", name="add_friend"),
)