from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.site.models import Site
from django.template import Context
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags


class NoticeCategory(models.Model):
    name = models.CharField("Name", max_length=50)

    def __unicode__(self):
        return self.name


class NoticeType(models.Model):
    label = models.CharField(_("label"), max_length=40)
    display = models.CharField(_("display"), max_length=50)
    enabled = models.BooleanField(_("enabled"), default=True)
    category = models.ForeignKey(NoticeCategory, related_name="notice_types")

    def __unicode__(self):
        return self.label

    class Meta:
        verbose_name = _("Notice Type")
        verbose_name_plural = _("Notice Types")


class Notice(models.Model):
    user = models.ForeignKey(User, verbose_name=_("user"))
    creator = models.ForeignKey(User, verbose_name=_(
        "creator"), null=True, related_name="created_notifications")
    message = models.TextField(_("message"))
    notice_type = models.ForeignKey(NoticeType, verbose_name=_("notice type"))
    created_at = models.DateTimeField(_("created_at"), auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __unicode__(self):
        return self.message


def get_formatted_messages(formats, label, context):
    """
    Returns a dictionary with the format identifier as the key. The values are
    are fully rendered templates with the given context.
    """
    format_templates = {}
    for format in formats:
    # conditionally turn off autoescaping for .txt extensions in format
        if format.endswith(".txt"):
            context.autoescape = False
        else:
            context.autoescape = True

        format_templates[format] = render_to_string((
            'notification/%s/%s' % (label, format),
            'notification/%s' % format), context_instance=context)

    return format_templates


def send(users, label, creator, extra_context=None):
    """
    function to send notif.
    """
    if extra_context is None:
        extra_context = {}

    notice_type = NoticeType.objects.get(label=label)
    formats = (
        "email_subject.txt",
        "email_content.html",
        "on_site_content.html",
    )

    current_site = Site.objects.get_current()
    for user in users:
        if not user:
            continue

        context = Context({
            "receiver": user,
            "current_site": current_site,
            "STATIC_URL": settings.STATIC_URL.
            "creator": creator,
            "avatar_url_prefix": settings.AVATAR_URL_PREFIX
        })

        messages = get_formatted_messages(formats, label, context)

        # create Notice object only for on site notif
        Notice.objects.create(
            user=user, message=messages['on_site_content.html'],
            notice_type=notice_type, creator=creator
        )

        body = messages['email_content.html']
        email = EmailMultiAlternatives(
            messages["email_subject.txt"], body, settings.DEFAULT_FROM_EMAIL, [user.email])
        email.attact_alternative(body, "text/html")
        email.send()
