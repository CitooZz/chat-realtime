from django.conf.urls import patterns, url

urlpatterns = patterns(
	"chat.apps.dashboard.views",

	url(r"$", "dashboard", name="dashboard"),
)