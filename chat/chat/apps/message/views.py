from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.contrib.auth.models import User
from chat.apps.message.models import Message, MessageStatus
from drealtime import iShoutClient
import json

ishout_client = iShoutClient()

@login_required
def messages(request):
    message_items = MessageStatus.objects.filter(
        user=request.user).order_by('-last_message__recieved_at')
    context = {
        'message_items': message_items,
        'users': User.objects.exclude(id=request.user.id)
    }
    return render(request, 'message/messages.html', context)


@login_required
def send_message(request, recipient_username):
    sender = request.user
    recipient = get_object_or_404(User, username=recipient_username)
    content = request.POST.get('message')
    message = Message(sender=sender, recipient=recipient, content=content)
    message.save()

    return HttpResponse(json.dumps({"success":True}), content_type="application/json")


@login_required
def get_conversation(request, contact_username):
    contact = get_object_or_404(User, username=contact_username)
    private_messages = Message.objects.messages_for(request.user, contact)

    private_messages.filter(recipient=request.user).update(is_read=True)
    context = {
        'private_messages': private_messages,
        'contact': contact
    }
    return render(request, 'message/get_conversation.html', context)