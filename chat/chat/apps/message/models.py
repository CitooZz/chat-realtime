from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from datetime import datetime
from drealtime import iShoutClient

ishout_client = iShoutClient()


class MessageManage(models.Manager):

    # Get messages including sent and received messages for a user.
    def messages_for(self, user, contact):
        return self.filter(Q(sender=contact, recipient=user, hide_for_recipient=False) |
                           Q(recipient=contact, sender=user, hide_for_sender=False)).order_by("received_at")


class Message(models.Model):

    """Private message between two contacts"""

    objects = MessageManage()

    sender = models.ForeignKey(User, related_name='sent_messages')
    recipient = models.ForeignKey(User, related_name='received_messages')
    received_at = models.DateTimeField(default=datetime.now())
    content = models.TextField()
    is_read = models.BooleanField(default=False)
    hide_for_sender = models.BooleanField(default=False)
    hide_for_recipient = models.BooleanField(default=False)

    def __unicode__(self):
        return self.content

    def save(self, *args, **kwargs):
        if not self.id:
            notifiy_users = [self.recipient.id, self.sender.id]
            for user_id in notifiy_users:
                ishout_client.emit(
                    int(user_id),
                    'messagechannel',
                    data={
                        'msg': self.content,
                        'sender': self.sender.id,
                        'recipient': self.recipient.id,
                        'sender_full_name': self.sender.profile.full_name,
                        'timestamp': datetime.strftime(self.received_at, "%b %d , %Y, %l:%M %P.")
                    }
                )
        super(Message, self).save(*args, **kwargs)


class MessageStatusManager(models.Manager):

    def create_or_update(self, sender, recipient, message):
        status = self.get_or_create(user=sender, contact=recipient)[0]
        status.last_message = message
        status.save()

        status_2 = self.get_or_create(user=recipient, contact=sender)[0]
        status_2.last_message = message
        status_2.save()


class MessageStatus(models.Model):
    objects = MessageStatusManager()

    user = models.ForeignKey(User, related_name='user_status')
    contact = models.ForeignKey(User, related_name='contact_status')
    last_message = models.ForeignKey(
        Message, related_name='message_status_items', null=True, blank=True)
