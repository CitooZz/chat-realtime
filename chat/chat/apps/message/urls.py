from django.conf.urls import patterns, url

urlpatterns = patterns(
	"chat.apps.message.views",

	url(r"^messages/$", "messages", name="messages"),
	url(r"^send_message/([\w\._-]+)/$", "send_message", name="send_message"),
	url(r"^get_conversation/([\w\._-]+)/$", "get_conversation", name="get_conversation")
)