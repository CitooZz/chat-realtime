from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User


class EmailAuthenticationBackend(ModelBackend):

    def authenticate(self, **credentials):
        try:
            user = User.objects.get(email__exact=credentials['username'])
            if user.check_password(credentials['password']):
                return user
        except User.DoesNotExist:
            return None
