from django.apps import AppConfig

class CustomAccount(AppConfig):
    name = 'chat.apps.account'
    label = 'custom_account'