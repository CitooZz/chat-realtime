from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from django.contrib.auth import logout as default_logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from django.contrib import messages
import account.forms
import account.views

from chat.apps.account.forms import SignupForm


class LoginView(account.views.LoginView):
    form_class = account.forms.LoginEmailForm


class SignupView(account.views.SignupView):
    form_class = SignupForm

    messages = {
        "email_confirmation_sent": {
        "level": messages.INFO,
        "text": _("Your ChitChat account has been created.")
        },
        "invalid signup code": {
        "level": messages.WARNING,
        "text": _("The code %(code)s is invalid.")
        }
    }

    def generate_username(self, form):
        # Username will be overwrite after signup. See the method below
        email = form.cleaned_data.get("email").strip()
        username = email.replace("@", "-").replace(".", "-")
        return username

    def after_signup(self, form):
        after_create_user(form, self.created_user)
        super(SignupView, self).after_signup(form)


def create_profile(form, user):
    profile = user.profile
    profile.full_name = form.cleaned_data.get("first_name") + " " + \
        form.cleaned_data["last_name"]
    profile.save()
    print profile.save()


def after_create_user(form, user):
    username = "%s-%s" % (slugify(form.cleaned_data["first_name"]), user.id)
    user.username = username
    user.save()
    create_profile(form, user)


@login_required
def logout(request):
    default_logout(request)
    return redirect(reverse("account_login"))
