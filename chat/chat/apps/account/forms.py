from django import forms
from django.utils.translation import ugettext_lazy as _

import account.forms


class SignupForm(account.forms.SignupForm):
    first_name = forms.CharField(
        label=_("First Name"), required=True, max_length=50)
    last_name = forms.CharField(
        label=_("Last Name"), required=True, max_length=50)

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        del self.fields['username']

    def clean_password(self):
    	password = self.cleaned_data.get('password')
    	if len(password) < 8:
    		raise forms.ValidationError('Password is too short. Make sure it has at least 8 characters.')
        return password
